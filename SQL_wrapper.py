import sqlite3, os

dbName = 'ppi.db'
comm_1 = 'sqlite3 %s ""' %(dbName)
os.system(comm_1)

conn = sqlite3.connect('ppi.db')
c = conn.cursor()

def tableCreate():
    c.execute('''CREATE TABLE TBL_12V
                (
                P1 INT,
                P2 INT,
                btw_P1 REAL,
                btw_P2 REAL,
                dgr_P1 INT,
                dgr_P2 INT,
                cls_P1 REAL,
                cls_P2 REAL,
                ecc_P1 INT,
                ecc_P2 INT,
                nb1_P1 INT,
                nb1_P2 INT,
                nb2_P1 INT,
                nb2_P2 INT,
                nb3_P1 INT,
                nb3_P2 INT,
                nb4_P1 INT,
                nb4_P2 INT,
                nb5_P1 INT,
                nb5_P2 INT,
                CC_P1 REAL,
                CC_P2 REAL,
                EvCV_P1 REAL,
                EvCV_P2 REAL,
                CNb_P1 INT,
                CNb_P2 INT
                )''')

def PP_data_Entry():
    F0 = open('./DATA/12V_PPI_test.txt')
    lines = F0.readlines()[1:]
    for ln in lines:
        tmp_lst = []
        ln = ln.rstrip('\n\r')
        ll = ln.split('\t')
        for i in ll:
            p1, p2 = i.split('|')
            tmp_lst.append(p1)
            tmp_lst.append(p2)

        c.execute('insert into TBL_12V values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', tmp_lst)
    conn.commit()

tableCreate()
PP_data_Entry()