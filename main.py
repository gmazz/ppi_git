import os, sys, re, sqlite3
from models import *
from fetching_db import *

def create_db():
    dbName = 'ppi.db'
    comm_1 = 'sqlite3 %s ""' %(dbName)
    os.system(comm_1)

root = './DATA'
#File list:
ppi_V12 = '%s/Sample05_Positive_pair_12vertex_properties_68265.txt' %root
nip_V12 = '%s/Sample04_Negative_pair_12vertex_properties_8519279.txt' %root
ppi_EigSim = '%s/Sample06_PPI_68285_Eigen-plus-similarity-3methods.txt' %root
nip_EigSim = '%s/Sample07_NIP_8519280_Eigen-plus-similarity-3methods.txt' %root
ppi_SPath = '%s/Sample08_PPI_68265_shortest.path.txt' %root
nip_SPath = '%s/Sample09_NIP_8519280_shortest.path.txt' %root
ppi_MRCorr = '%s/Sample10_Positive_MR-COR_68265.txt' %root
nip_MRCorr = '%s/Sample11_NIP_MR_COR_.420_13480146.txt' %root
ppi_Espritz = '%s/Sample12_PPI_espritz_mapping_verified.txt' %root
nip_Espritz = '%s/Sample13_Negative_espritz_mapping_verified.txt' %root
ppi_DDIProp = '%s/Sample16short_PPI_domain_properties_55968_verified_with_nddi.txt' %root
nip_DDIProp = '%s/Sample17_NIP_domain_properties_7571294_verified_with_pddi-wonddiprops.txt' %root
ppi_Pairs = '%s/Sample18_PPI_pair_latest.txt' %root
nip_Pairs = '%s/Sample19_NIP_pair_latest.txt' %root

def iterate():

    # File dict:
    # file_name_variable: [models.schema.class, 'db-table-name']

    f_dict = {
              ppi_V12: [SchV12,'PPI_V12'],
              nip_V12: [SchV12,'NIP_V12'],
              ppi_EigSim: [SchEigSim,'PPI_EigSim'],
              nip_EigSim: [SchEigSim,'NIP_EigSim'],
              ppi_SPath: [SchSPath,'PPI_SPath'],
              nip_SPath: [SchSPath,'NIP_SPath'],
              ppi_MRCorr: [SchMRCorr,'PPI_MRCorr'],
              nip_MRCorr: [SchMRCorr,'NIP_MRCorr'],
              ppi_Espritz: [SchEspritz,'PPI_Espritz'],
              nip_Espritz: [SchEspritz,'NIP_Espritz'],
              ppi_DDIProp: [SchDDIProp,'PPI_DDIProp'],
              nip_DDIProp: [SchDDIProp,'NIP_DDIProp'],
              ppi_Pairs: [SchPairs,'PPI_Pairs'],
              nip_Pairs: [SchPairs,'NIP_Pairs']
             }

    for f, sch in f_dict.iteritems():
        tmp_obj = sch[0](f, sch[1])
        tmp_obj.CreateTable()
        tmp_obj.DBInsert()

def generate_db():
    create_db()
    iterate()

## Genarating and filling the final table:

def gen_table():
    fetch_obj = selTable() #Fatch all the possible info from ppi.db (as defined in fetching_db.py)
    final_table_name = 'TB'
    table = FinalTable(final_table_name, fetch_obj)
    table.create_table()
    return table

def fill_table(table):
    table.fill_table()

generate_db()
table = gen_table()
fill_table(table)