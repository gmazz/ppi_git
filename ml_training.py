import aaindex
import numpy as np
import pandas as pd
import argparse
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import precision_score, recall_score

# All tested classifiers should follow sklearn interface with .fit and .predict
# methods. Custom preprocessing etc. should be implemented as a wrapper object
# exposing .fit and .predict methods.
clf = LogisticRegression()

# AA descriptors. TODO: To be moved to a config file.
descriptors = ["RADA880102", "FAUJ880103", "ZIMJ680104", "GRAR740102", "CRAJ730103", "BURA740101", "CHAM820102"]

def get_aaindex_feature(amino_acid, aai_record, seq):
    # B and Z are ambiguous amino acids.
    if amino_acid == "B":
        val = (aai_record.get("D") + aai_record.get("N")) / 2
    elif amino_acid == "Z":
        val = (aai_record.get("E") + aai_record.get("Q")) / 2
    elif amino_acid == "O":
        val = aai_record.get("K")
    elif amino_acid == "U":
        val = aai_record.get("C")
    elif amino_acid in "X*-":
        val = 0.0
    else:
        val = aai_record.get(amino_acid)
    # Checking for "None" type in case of an unspecified amino acid character.
    if type(val) != type(0.0):
        print("Unrecognised amino acid symbol " + amino_acid + " found in sequence " + seq)
        exit(-1)
    return val

def encode_aaindex_features(sequence_window):
    aaindex.init(path='.', index='1')
    aai_recs = [aaindex.get(d) for d in descriptors]
    return np.array([[get_aaindex_feature(aa, r, seq) for aa in seq
        for r in aai_recs] for seq in sequence_window])

def test_clf(clf, X, Y):
    cv = StratifiedKFold(Y, n_folds=10)
    precision_scores = []
    recall_scores = []
    for train, test in cv:
        clf.fit(X[train], Y[train])
        Yp = clf.predict(X[test])
        precision_scores.append(precision_score(Y[test], Yp))
        recall_scores.append(recall_score(Y[test], Yp))
        print("Positive prediction: " + str(sum(Yp)))
        print("Negative prediction: " + str(len(test) - sum(Yp)))

    print(np.mean(precision_scores), np.mean(recall_scores))



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Training and testing classifiers. If no options are present, classifier evaluation is performed.')
    parser.add_argument('data_file', help='file containing window data')
    parser.add_argument('-s', '--save_classifier', help='save trained classifier in file')
    parser.add_argument('-p', '--predict', help='use classifier to make predictions')
    args = parser.parse_args()

    data = pd.read_csv(args.data_file, sep=",")
    sequences = data['Sequence_window']
    try:
        Y = data['Positive']
    except KeyError:
        if not args.predict:
            print("Missing class values.")
            exit(1)

    X = encode_aaindex_features(sequences)

    print(X[1:3])

    if args.predict:
        clf = joblib.load(args.predict)
        print("\n".join(str(p) for p in clf.predict(X)))
    elif args.save_classifier:
        clf.fit(X,Y)
        joblib.dump(clf, args.save_classifier)
    else:
        test_clf(clf, X, Y)
