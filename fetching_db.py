import sqlite3, scipy
import numpy as np

conn = sqlite3.connect('ppi.db')
c = conn.cursor()

class selTable(object):

    def ppi_pos(self):
        self.command_pos = '''
                  select
                         PPI_Pairs.P1,
                         PPI_Pairs.P2,

                         PPI_DDIProp.numDomainP1,
                         PPI_DDIProp.numDomainP2,
                         PPI_DDIProp.numDDI,
                         PPI_DDIProp.Max_freq_DDI,
                         PPI_DDIProp.Min_freq_DDI,
                         PPI_DDIProp.Name_D1_of_Max_freq,
                         PPI_DDIProp.Name_D2_of_Max_freq,
                         PPI_DDIProp.Min_Zscore,
                         PPI_DDIProp.Max_Zscore,
                         PPI_DDIProp.Max_Zscore_NDDI,
                         PPI_DDIProp.Neg_Zscore,
                         PPI_DDIProp.Neg_freq,

                         PPI_V12.btw_P1,
                         PPI_V12.btw_P2,
                         PPI_V12.dgr_P1,
                         PPI_V12.dgr_P2,
                         PPI_V12.cls_P1,
                         PPI_V12.cls_P2,
                         PPI_V12.ecc_P1,
                         PPI_V12.ecc_P2,
                         PPI_V12.nb1_P1,
                         PPI_V12.nb1_P2,
                         PPI_V12.nb2_P1,
                         PPI_V12.nb2_P2,
                         PPI_V12.nb3_P1,
                         PPI_V12.nb3_P2,
                         PPI_V12.nb4_P1,
                         PPI_V12.nb4_P2,
                         PPI_V12.nb5_P1,
                         PPI_V12.nb5_P2,
                         PPI_V12.CC_P1,
                         PPI_V12.CC_P2,
                         PPI_V12.EvCV_P1,
                         PPI_V12.EvCV_P2,

                         PPI_EigSim.Eigen_value,
                         PPI_EigSim.Similarity_Jaccard,
                         PPI_EigSim.Similarity_dice,
                         PPI_EigSim.Similarity_inverse_log_weighted,

                         PPI_Espritz.totalAA_p1,
				         PPI_Espritz.totalAA_p2,
                         PPI_Espritz.disorder_p1,
                         PPI_Espritz.disorder_p2,
                         PPI_Espritz.greater_than_30aa_p1,
                         PPI_Espritz.greater_than_30aa_p2,
                         PPI_Espritz.greater_than_50aa_p1,
                         PPI_Espritz.greater_than_50aa_p2,
                         PPI_Espritz.disordered_segments_p1,
                         PPI_Espritz.disordered_segments_p2,

                         PPI_MRCorr.MR,
                         PPI_MRCorr.COR,

                         PPI_SPath.Shortest_path

                  from
                      PPI_Pairs,
                      PPI_DDIProp,
                      PPI_V12,
                      PPI_EigSim,
                      PPI_Espritz,
                      PPI_MRCorr,
                      PPI_SPath

                  ON
                    PPI_Pairs.P1=PPI_DDIProp.P1 AND PPI_Pairs.P2=PPI_DDIProp.P2 AND
                    PPI_Pairs.P1=PPI_V12.P1 AND PPI_Pairs.P2=PPI_V12.P2 AND
                    PPI_Pairs.P1=PPI_EigSim.P1 AND PPI_Pairs.P2=PPI_EigSim.P2 AND
                    PPI_Pairs.P1=PPI_Espritz.P1 AND PPI_Pairs.P2=PPI_Espritz.P2 AND
                    PPI_Pairs.P1=PPI_MRCorr.P1 AND PPI_Pairs.P2=PPI_MRCorr.P2 AND
                    PPI_Pairs.P1=PPI_SPath.P1 AND PPI_Pairs.P2=PPI_SPath.P2
                  '''

        c.execute(self.command_pos)
        c_pos = c.fetchall()
        return c_pos

    def ppi_neg(self):
        self.command_neg = '''
                  select
                         NIP_Pairs.P1,
                         NIP_Pairs.P2,

                         NIP_DDIProp.numDomainP1,
                         NIP_DDIProp.numDomainP2,
                         NIP_DDIProp.numDDI,
                         NIP_DDIProp.Max_freq_DDI,
                         NIP_DDIProp.Min_freq_DDI,
                         NIP_DDIProp.Name_D1_of_Max_freq,
                         NIP_DDIProp.Name_D2_of_Max_freq,
                         NIP_DDIProp.Min_Zscore,
                         NIP_DDIProp.Max_Zscore,
                         NIP_DDIProp.Max_Zscore_NDDI,
                         NIP_DDIProp.Neg_Zscore,
                         NIP_DDIProp.Neg_freq,

                         NIP_V12.btw_P1,
                         NIP_V12.btw_P2,
                         NIP_V12.dgr_P1,
                         NIP_V12.dgr_P2,
                         NIP_V12.cls_P1,
                         NIP_V12.cls_P2,
                         NIP_V12.ecc_P1,
                         NIP_V12.ecc_P2,
                         NIP_V12.nb1_P1,
                         NIP_V12.nb1_P2,
                         NIP_V12.nb2_P1,
                         NIP_V12.nb2_P2,
                         NIP_V12.nb3_P1,
                         NIP_V12.nb3_P2,
                         NIP_V12.nb4_P1,
                         NIP_V12.nb4_P2,
                         NIP_V12.nb5_P1,
                         NIP_V12.nb5_P2,
                         NIP_V12.CC_P1,
                         NIP_V12.CC_P2,
                         NIP_V12.EvCV_P1,
                         NIP_V12.EvCV_P2,

                         NIP_EigSim.Eigen_value,
                         NIP_EigSim.Similarity_Jaccard,
                         NIP_EigSim.Similarity_dice,
                         NIP_EigSim.Similarity_inverse_log_weighted,

                         NIP_Espritz.totalAA_p1,
				         NIP_Espritz.totalAA_p2,
                         NIP_Espritz.disorder_p1,
                         NIP_Espritz.disorder_p2,
                         NIP_Espritz.greater_than_30aa_p1,
                         NIP_Espritz.greater_than_30aa_p2,
                         NIP_Espritz.greater_than_50aa_p1,
                         NIP_Espritz.greater_than_50aa_p2,
                         NIP_Espritz.disordered_segments_p1,
                         NIP_Espritz.disordered_segments_p2,

                         NIP_MRCorr.MR,
                         NIP_MRCorr.COR,

                         NIP_SPath.Shortest_path

                  from
                      NIP_Pairs,
                      NIP_DDIProp,
                      NIP_V12,
                      NIP_EigSim,
                      NIP_Espritz,
                      NIP_MRCorr,
                      NIP_SPath

                  ON
                    NIP_Pairs.P1=NIP_DDIProp.P1 AND NIP_Pairs.P2=NIP_DDIProp.P2 AND
                    NIP_Pairs.P1=NIP_V12.P1 AND NIP_Pairs.P2=NIP_V12.P2 AND
                    NIP_Pairs.P1=NIP_EigSim.P1 AND NIP_Pairs.P2=NIP_EigSim.P2 AND
                    NIP_Pairs.P1=NIP_Espritz.P1 AND NIP_Pairs.P2=NIP_Espritz.P2 AND
                    NIP_Pairs.P1=NIP_MRCorr.P1 AND NIP_Pairs.P2=NIP_MRCorr.P2 AND
                    NIP_Pairs.P1=NIP_SPath.P1 AND NIP_Pairs.P2=NIP_SPath.P2
                  '''
        c.execute(self.command_neg)
        c_neg = c.fetchall()
        return c_neg

        #ppi_data = scipy.array(c.fetchall())
        #print len(ppi_data[0])

class FinalTable(object):

    def __init__(self, name, fetch_obj):
        self.name = name
        self.fetch_obj = fetch_obj

    def create_table(self):
        drop_table = 'DROP TABLE IF EXISTS %s' %self.name
        c.execute(drop_table) # Drops the existing table

        c.execute('''CREATE TABLE ''' + self.name +
                  '''(
                    P1 INT,
                    P2 INT,

                    numDomainP1 INT,
                    numDomainP2 INT,
                    numDDI INT,
                    Max_freq_DDI REAL,
                    Min_freq_DDI REAL,
                    Name_D1_of_Max_freq TEXT,
                    Name_D2_of_Max_freq TEXT,
                    Min_Zscore REAL,
                    Max_Zscore REAL,
                    Max_Zscore_NDDI REAL,
                    PDDI_Zscore REAL,
                    PDDI_freq REAL,

                    btw_P1 REAL,
                    btw_P2 REAL,
                    dgr_P1 INT,
                    dgr_P2 INT,
                    cls_P1 REAL,
                    cls_P2 REAL,
                    ecc_P1 INT,
                    ecc_P2 INT,
                    nb1_P1 INT,
                    nb1_P2 INT,
                    nb2_P1 INT,
                    nb2_P2 INT,
                    nb3_P1 INT,
                    nb3_P2 INT,
                    nb4_P1 INT,
                    nb4_P2 INT,
                    nb5_P1 INT,
                    nb5_P2 INT,
                    CC_P1 REAL,
                    CC_P2 REAL,
                    EvCV_P1 REAL,
                    EvCV_P2 REAL,

                    Eigen_value REAL,
                    Similarity_Jaccard REAL,
                    Similarity_dice REAL,
                    Similarity_inverse_log_weighted REAL,

                    totalAA_p1 INT,
                    totalAA_p2 INT,
                    disorder_p1 REAL,
                    disorder_p2 REAL,
                    greater_than_30aa_p1 INT,
                    greater_than_30aa_p2 INT,
                    greater_than_50aa_p1 INT,
                    greater_than_50aa_p2 INT,
                    disordered_segments_p1 INT,
                    disordered_segments_p2 INT,

                    MR REAL,
                    COR REAL,

                    Shortest_path INT,

                    Type INT,

                    PRIMARY KEY (P1, P2)

                    )''')

    def fill_table(self):

        list_pos = self.fetch_obj.ppi_pos()
        list_neg = self.fetch_obj.ppi_neg()

        for tmp_lp in list_pos:
            tmp_lp += (+1,)
            tmp_lp_len = len(tmp_lp)
            qm_lp = ",".join("?"*tmp_lp_len)
            val_string = 'insert into '+ self.name +' values(%s)' %qm_lp
            c.execute(val_string, tmp_lp)
            conn.commit()

        for tmp_ln in list_neg:
            tmp_ln += (-1,)
            tmp_ln_len = len(tmp_ln)
            qm_ln = ",".join("?"*tmp_ln_len)
            val_string = 'insert into '+ self.name +' values(%s)' %qm_ln
            c.execute(val_string, tmp_ln)
            conn.commit()
