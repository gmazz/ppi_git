import sqlite3, os

conn = sqlite3.connect('ppi.db')
c = conn.cursor()

class Insert(object):

    def __init__(self, file, fn):
        self.file = file
        self.fn = fn

    def DBInsert(self):
        foo = open(self.file)
        lines = foo.readlines()[0:]
        for ln in lines:
            tmp_lst = []
            ln = ln.rstrip('\n\r\t')
            ll = ln.split('\t')
            for i in ll:
                if '|' in i:
                    p1, p2 = i.split('|')
                    tmp_lst.append(p1)
                    tmp_lst.append(p2)
                else:
                    tmp_lst.append(i)

            lo = len(tmp_lst)
            qm_rep = ",".join("?"*lo)
            val_string = 'insert into '+ self.fn +' values(%s)' %qm_rep
            c.execute(val_string, tmp_lst)

        conn.commit()


class SchV12(Insert):

    def CreateTable(self):
        c.execute('''CREATE TABLE ''' + self.fn +
                  '''(
                    P1 INT,
                    P2 INT,
                    btw_P1 REAL,
                    btw_P2 REAL,
                    dgr_P1 INT,
                    dgr_P2 INT,
                    cls_P1 REAL,
                    cls_P2 REAL,
                    ecc_P1 INT,
                    ecc_P2 INT,
                    nb1_P1 INT,
                    nb1_P2 INT,
                    nb2_P1 INT,
                    nb2_P2 INT,
                    nb3_P1 INT,
                    nb3_P2 INT,
                    nb4_P1 INT,
                    nb4_P2 INT,
                    nb5_P1 INT,
                    nb5_P2 INT,
                    CC_P1 REAL,
                    CC_P2 REAL,
                    EvCV_P1 REAL,
                    EvCV_P2 REAL,
                    CNb_P1 INT,
                    CNb_P2 INT,
                    PRIMARY KEY (P1, P2)
                    )''')

class SchEigSim(Insert):

    def CreateTable(self):
        c.execute('''CREATE TABLE ''' + self.fn +
                  '''(
                    P1 INT,
                    P2 INT,
                    Eigen_value REAL,
                    Similarity_Jaccard REAL,
                    Similarity_dice REAL,
                    Similarity_inverse_log_weighted REAL,
                    PRIMARY KEY (P1, P2)
                    )''')

class SchSPath(Insert):

    def CreateTable(self):
        c.execute('''CREATE TABLE ''' + self.fn +
                '''(
                P1 INT,
                P2 INT,
                Shortest_path INT,
                PRIMARY KEY (P1, P2)
                )''')

class SchMRCorr(Insert):

    def CreateTable(self):
        c.execute('''CREATE TABLE ''' + self.fn +
                '''(
                P1 INT,
                P2 INT,
                MR REAL,
				COR REAL,
				PRIMARY KEY (P1, P2)
                )''')

class SchEspritz(Insert):

    def CreateTable(self):
        c.execute('''CREATE TABLE ''' + self.fn +
                '''(
                P1 INT,
                P2 INT,
                totalAA_p1 INT,
				totalAA_p2 INT,
				disorder_p1 REAL,
				disorder_p2 REAL,
				greater_than_30aa_p1 INT,
				greater_than_30aa_p2 INT,
				greater_than_50aa_p1 INT,
				greater_than_50aa_p2 INT,
				disordered_segments_p1 INT,
				disordered_segments_p2 INT,
				PRIMARY KEY (P1, P2)
                )''')

class SchDDIProp(Insert):

    def CreateTable(self):
        c.execute('''CREATE TABLE ''' + self.fn +
                '''(
                P1 INT,
                P2 INT,
                numDomainP1 INT,
				numDomainP2 INT,
				numDDI INT,
				Max_freq_DDI REAL,
				Min_freq_DDI REAL,
				Name_D1_of_Max_freq TEXT,
				Name_D2_of_Max_freq TEXT,
				Min_Zscore REAL,
				Max_Zscore REAL,
				Max_Zscore_NDDI REAL,
				Neg_Zscore REAL,
				Neg_freq REAL,
				PRIMARY KEY (P1, P2)
                )''')

class SchPairs(Insert):

    def CreateTable(self):
        c.execute('''CREATE TABLE ''' + self.fn +
                '''(
                P1 INT,
                P2 INT,
                PRIMARY KEY (P1, P2)
                )''')